var AppConstants = require('../constants/AppConstants');

var _pendingRequests = {};

var API_URL = 'http://192.168.137.230:9090/JGGFrame/a/cashier/';
var TIMEOUT = 10000;

function token() {
    return "test"; // TODO authentication with using AuthStore.getState().token;
}

function makeUrl(part) {
    return API_URL + part;
}
function timeout(){

}

var Api = {
    http:function(part,method,params,fn){
        var url=makeUrl(part);
        $.ajax({
            url:url,
            type:method,
            cache:false,
            data:params,
            /*
            headers: {
               'authtoken': token()
            },*/
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            timeout:20000,
            beforeSend:function(){

            },
            success:function(data) {
              
              if(typeof data!="object"){
                  data=eval("("+data+")");
              }

              if(data.resultCode!="0000"){
                   alert(data.resultMsg);
                   return;
              }
              fn(data);
            },
            error:function() {
                alert("网络请求失败，请稍后重试")
            },
            complete:function(){

            }
        });
   },
   login:function(params,fn){
     this.http("login","post",params,fn);
   },
   signout:function(params,fn){
     this.http("logout","post",params,fn);
   },
   //商品查询
   productsQuery:function(params,fn){
     this.http("productsQuery","get",params,fn);
   },
   //会员信息查询
   memberInfoQuery:function(params,fn){
     this.http("leaguerInfoQuery","get",params,fn);
   },
   //挂单
   hangOrder:function(params,fn){
     this.http("hangOrder.do","post",params,fn);
   },
   //提单列表
   pickupListQuery:function(params,fn){
     this.http("hangOrderListQuery.do","get",params,fn);
   },
   //提单详情
   pickupDetailQuery:function(params,fn){
     this.http("hangOrderDetailQuery.do","get",params,fn);
   },
   settlementSaleOrder:function(params,fn){
     this.http("settlementSaleOrder.do","post",params,fn);
   }
};

module.exports = Api;
