
var currentLine=-1,offsetTr=0;
var $=function(id){
	return document.getElementById(id);
}
function keyDownEvent(e){
	var e=window.event||e;
	if(e.keyCode==38){
		offsetTr=0;
		currentLine--;
		changeItem();
	}else if(e.keyCode==40){
		offsetTr=150;
		currentLine++;
		changeItem();
	}else if(e.keyCode==13&&currentLine>-1){
		addUser();
	}
	return false;
}
function changeItem(){
	if(!$('buddyListTable')) return false;
	var it=$('buddyListTable');
	if(document.all){
		it=$('buddyListTable').children[0];
	}
	changeBackground();
	if(currentLine<0) currentLine=it.rows.length-1;
	if(currentLine>=it.rows.length) currentLine=0;
	it.rows[currentLine].className="buddyListHighLight";
	if($('allBuddy')){
		$('allBuddy').scrollTop=it.rows[currentLine].offsetTop-offsetTr;
	}
}
function changeBackground(){
	var it=$('buddyListTable');
	if(document.all){
		it=$('buddyListTable').children[0];
	}
	for (var i = 0;i<it.rows.length;i++) {
		if(i%2==0){
			it.rows[i].className="buddyListOdd";
		}else{
			it.rows[i].className="buddyListEven";
		}
	}
}
function addUser(){
	var it=$('buddyListTable');
	if(document.all){
		it=$('buddyListTable').children[0];
	}
	var trBody=it.rows[currentLine].innerHTML;
	$('result').innerHTML=$('result').innerHTML+trBody;
}