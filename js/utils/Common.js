
var Common = {
	setCookie:function(name,value){
		var Days = 30;
		var exp = new Date();
		exp.setTime(exp.getTime() + Days*24*60*60*1000);
		document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
	},
	getCookie:function(name){
		var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
		if(arr=document.cookie.match(reg))
			return unescape(arr[2]);
		else
			return null;
	},
	delCookie:function(name){
		var exp = new Date();
		exp.setTime(exp.getTime() - 1);
		var cval=getCookie(name);
		if(cval!=null)
		document.cookie= name + "="+cval+";expires="+exp.toGMTString();
	},
   print:function(products,checkout,isMember){
   	var username=this.getCookie("USER");

   	var storename=this.getCookie("STORENAME");
   	var backMoney=this.subtr(checkout['handMoney'],checkout['promotionPrice']);
   	var html="<table style='font-size:20px'><tr><td width='190' align='center'>优客优奇</td></tr></table>"+
				"<table><tr><td width='80'>&nbsp;&nbsp;收银员："+username+"</td><td width='110'>店名："+storename+"</td></tr></table>"+
				"<table><tr><td width='190'>&nbsp;&nbsp;时间："+this.dateFormat(new Date(),"yyyy-MM-dd hh:mm")+"</td></tr></table>"+
				"<table>"+
					"<tr>"+
						"<td width='130' align='left' style='padding-left:12px;'>商品名称</td>"+
						"<td width='30' align='left'>数量</td>"+
						"<td width='30' align='left'>单价</td>"+
					"</tr>";
	for (var i =0; i<products.length;i++) {
		var product=products[i];
		var a=product.amount!=undefined?product.amount:1;
		var price=product.retailPrice;
		if(isMember&&product.activityPrice!=undefined&&product.activityPrice!=0){
			price=product.activityPrice;
		}
		html+="<tr>"+
				"<td width='130' align='left' style='padding-left:12px;'>"+product.productName+"</td>"+
				"<td width='30' align='left'>"+a+"</td>"+
				"<td width='30' align='left'>"+(price)+"</td>"+
			"</tr>";
	}				
	html+="</table>"+
			"<table>"+
				"<tr>"+
					"<td width='90' align='left'>&nbsp;&nbsp;件数："+checkout['amount']+"</td>"+
					"<td width='100' align='left'>&nbsp;&nbsp;金额："+checkout['promotionPrice']+"</td>"+
				"</tr>"+
			"</table>"+
			"<table>"+
				"<tr>"+
					"<td width='90' align='left'>&nbsp;&nbsp;应收："+checkout['promotionPrice']+"</td>"+
					"<td width='100' align='left'>&nbsp;&nbsp;实收："+checkout['handMoney']+"</td>"+
				"</tr>"+
				"<tr>"+
					"<td colspan='2' width='190' align='left'>&nbsp;&nbsp;找零："+backMoney+"</td>"+
				"</tr>"+
			"</table>"+

			"<table>"+
				"<tr>"+
					"<td width='190' align='center'>&nbsp;&nbsp;</td>"+
				"</tr>"+
			"</table>"+
			"<table>"+
				"<tr>"+
					"<td width='190' align='center'>欢迎下次光临</td>"+
				"</tr>"+
			"</table>"+
			"<table>"+
				"<tr>"+
					"<td width='190' align='center'>&nbsp;&nbsp;</td>"+
				"</tr>"+
			"</table>";
	return html;
   },
	dateFormat:function (date,fmt) { //author: meizz 
	    var o = {
	        "M+": date.getMonth() + 1, //月份 
	        "d+": date.getDate(), //日 
	        "h+": date.getHours(), //小时 
	        "m+": date.getMinutes(), //分 
	        "s+": date.getSeconds(), //秒 
	        "q+": Math.floor((date.getMonth() + 3) / 3), //季度 
	        "S": date.getMilliseconds() //毫秒 
	    };
	    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
	    for (var k in o)
	    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	    return fmt;
	},
   //加法函数  
   accAdd:function(arg1, arg2) {  
	    var r1, r2, m;  
	    try {  
	        r1 = arg1.toString().split(".")[1].length;  
	    }  
	    catch (e) {  
	        r1 = 0;  
	    }  
	    try {  
	        r2 = arg2.toString().split(".")[1].length;  
	    }  
	    catch (e) {  
	        r2 = 0;  
	    }  
	    m = Math.pow(10, Math.max(r1, r2));  
	    return (arg1 * m + arg2 * m) / m;  
	},
	//减法函数  
	subtr:function(arg1, arg2) {  
	    var r1, r2, m, n;  
	    try {  
	        r1 = arg1.toString().split(".")[1].length;  
	    }  
	    catch (e) {  
	        r1 = 0;  
	    }  
	    try {  
	        r2 = arg2.toString().split(".")[1].length;  
	    }  
	    catch (e) {  
	        r2 = 0;  
	    }  
	    m = Math.pow(10, Math.max(r1, r2));  
	     //last modify by deeka  
	     //动态控制精度长度  
	    n = (r1 >= r2) ? r1 : r2;  
	    return ((arg1 * m - arg2 * m) / m).toFixed(n);  
	},
	//乘法函数  
	accMul:function(arg1, arg2) {  
	    var m = 0, s1 = arg1.toString(), s2 = arg2.toString();  
	    try {  
	        m += s1.split(".")[1].length;  
	    }  
	    catch (e) {  
	    }  
	    try {  
	        m += s2.split(".")[1].length;  
	    }  
	    catch (e) {  
	    }  
	    return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m);  
	},
	//除法函数  
	accDiv:function(arg1, arg2) {  
	    return 1;
	}    
};

module.exports = Common;