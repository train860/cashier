import React from 'react';
import ReactDOM from 'react-dom';
import Common from '../utils/Common';
import Api from '../network/DataService';
export default class Login extends React.Component {
  handleLogin(){
    var params={
       username:this.refs.username.value,
       password:this.refs.password.value,
       type:'cashier',
    }

    Api.login(params,function(result){
  			 if(result.resultCode!="0000"){
           alert("用户名或密码错误");
           return;
         }
         Common.setCookie("USER",result.content.name);
         Common.setCookie("USERID",result.content.id);
         Common.setCookie("STORENAME",result.content.organName);
         location.href="index.html";
		}.bind(this));
  }
  componentDidMount(){
      window.requestAnimationFrame(function() {
          $("#sign_in").css("margin-top",($(document).height()-$("#sign_in").height())/2);
      })
  }
  render() {
    return(
      <div className="sign_in" id="sign_in">
    		<h2>优客优奇连锁便利店</h2>
    		<div>
    			<label>帐 号：</label>
    			<input className="form-control" ref="username"/>
    			<span></span>
    		</div>
    		<div>
    			<label>密 码：</label>
    			<input type="password" className="form-control" ref="password"/>
    		</div>
    		<button className="btn btn-primary" onClick={this.handleLogin.bind(this)}> 登 录 </button>
    	</div>
    )
  }
}
ReactDOM.render(
  <Login/>,
  document.getElementById('login-view')
);
