import React from 'react';
import Table from './Table';
export default class BillElement extends React.Component {
	handleSelect(index){
		this.props.handleSelectBill(index);
	}
	componentDidUpdate(){
       
	}
	componentDidMount(){
       
	}
	render() {
		var i=0;
	
		var bills=this.props.data.map(function(bill){
			i++;
			return(
				<tr>
					<td>{i}</td>
					<td>{bill.createDate}</td>
					<td>{bill.hangId}</td>
					<td>{bill.staffId}</td>
				</tr>
			)
		}.bind(this));
		return(
			<Table onCheck={this.handleSelect.bind(this)}>
				<thead>
					<tr>
						<td>序号</td>
						<td>创建时间(挂单时间)</td>
						<td>挂单单号</td>
						<td>员工ID</td>
					</tr>
				</thead>
				<tbody id="buddyListTable">
					{bills}
				</tbody>
			</Table>
		)
	}
}