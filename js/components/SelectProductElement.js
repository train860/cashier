import React from 'react';
import Table from './Table';
import Widget from '../utils/Widget';

export default class SelectProductElement extends React.Component {
	handleSelect(index){
		this.props.handleSelectProduct(index);
	}
	
	componentDidUpdate(){
       
	}
	componentDidMount(){
		window.requestAnimationFrame(function() {
			Widget.resetModal();
	   }.bind(this));
	}
	render() {
		var i=0;
	
		var products=this.props.data.map(function(product){
			i++;
			return(
				<tr>
					<td>{i}</td>
					<td>{product.productName}</td>
					<td>{product.standard}</td>
					<td>{product.modelNum}</td>
					<td>{product.amount!=undefined?product.amount:1}</td>
					<td>{product.retailPrice}</td>
					<td>{product.activityPrice}</td>
					<td>{product.promotionPrice}</td>
					<td>{product.unit}</td>
					<td>{product.barcode}</td>
					<td>{product.productId}</td>
				</tr>
			)
		}.bind(this));
		return(
			<Table onCheck={this.handleSelect.bind(this)}>
				<thead>
					<tr>
						<td>序号</td>
						<td>商品名称</td>
						<td>规格</td>
						<td>型号</td>
						<td>数量</td>
						<td>零售价</td>
						<td>活动价</td>
						<td>促销价</td>
						<td>单位</td>
						<td>条形码</td>
						<td>商品编号</td>
					</tr>
				</thead>
				<tbody id="buddyListTable">
					{products}
				</tbody>
			</Table>
		)
	}
}