import React from 'react';
export default class Nav extends React.Component {
	handleAction(index){
		this.props.handleAction(index);
	}
	componentDidMount(){
       
	}
	render() {
		return(
			<div id="menu" className="right aside">
				<button id="Membership_identification" onClick={this.handleAction.bind(this,0)}>会员识别</button>
				<button onClick={this.handleAction.bind(this,1)}>数量增加</button>
				<button onClick={this.handleAction.bind(this,2)}>数量减少</button>
				<button onClick={this.handleAction.bind(this,3)}>单品删除</button>
				<button onClick={this.handleAction.bind(this,4)}>整单删除</button>
				<button onClick={this.handleAction.bind(this,5)}>挂单</button>
				<button onClick={this.handleAction.bind(this,6)}>提单</button>
			</div>
		)
	}
}