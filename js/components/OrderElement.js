import React from 'react';
import KeyHandler, {KEYPRESS} from 'react-key-handler';
import Widget from '../utils/Widget';
import Common from '../utils/Common';

export default class OrderElement extends React.Component {

	onKeyEnter(e){
		var code=event.keyCode||event.whitch;
		if(code==13){
			this.props.handleOrderDone();
		}else if(code==46){
			this.props.handleOrderBack();
		}
	}
	constructor(props) {
		super(props);
	    this.state = {};
	    this.bound_onKeyEnter = this.onKeyEnter.bind(this);
  	}
	componentDidMount(){
		window.addEventListener("keyup",this.bound_onKeyEnter,false);
		window.requestAnimationFrame(function() {
			Widget.resetModal();
	   }.bind(this));
		//更新屏显
		if(typeof(python)!="undefined"){
		 	python.displayBackPrice(Common.subtr(this.props.data.amount,this.props.data.shouldAmount));
		}
	}
	componentWillUpdate(nextProps,nextState){
		//更新屏显
		if(typeof(python)!="undefined"){
	  		python.displayBackPrice(Common.subtr(this.props.data.amount,this.props.data.shouldAmount));
	  	}
  	}
	componentWillUnmount(){
		window.removeEventListener("keyup",this.bound_onKeyEnter,false);
	}
	
	render() {
		var backMoney=Common.subtr(this.props.data.amount,this.props.data.shouldAmount);
		return(
			<div className="payment_method">
				<h2>现金结算收银</h2>
				<table>
					<tbody>
						<tr>
							<td>找零：</td>
							<td>
								<span>{backMoney}元</span>
							</td>
						</tr>

						<tr>
							<td>订单号：</td>
							<td>
								<span>{this.props.data.orderId}</span>
							</td>
						</tr>
						<tr>
							<td>应收金额：</td>
							<td>
								<span>{this.props.data.shouldAmount}元</span>
							</td>
						</tr>
						<tr>
							<td>实收金额：</td>
							<td>
								<span>{this.props.data.amount}元</span>
							</td>
						</tr>
						<tr>
							<td>已优惠：</td>
							<td>
								<span>{this.props.data.discountPrice}元</span>
							</td>
						</tr>
					</tbody>
				</table>
				<p>DELETE返回，ENTER确认</p>
			</div>
		)
	}
}