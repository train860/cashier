import React from 'react';
import KeyHandler, {KEYPRESS} from 'react-key-handler';

var currentLine=-1,offsetTr=0;

var $=function(id){
	return document.getElementById(id);
}

export default class Table extends React.Component {

	onKeyUp(){
		offsetTr=0;
		currentLine--;
		this.changeItem();
	}
	onKeyDown(){
		offsetTr=150;
		currentLine++;
		this.changeItem();
	}
	onKeyEnter(){
		console.log('onKeyEnter');
		if(currentLine>-1){
			this.props.onCheck(currentLine);
			currentLine=-1;
		}
	}
	changeItem(){
		if(!$('buddyListTable')) return false;
		var it=$('buddyListTable');
		if(document.all){
			it=$('buddyListTable').children[0];
		}
		this.changeBackground();
		if(currentLine<0) currentLine=it.rows.length-1;
		if(currentLine>=it.rows.length) currentLine=0;
		it.rows[currentLine].className="buddyListHighLight";
		if($('allBuddy')){
			$('allBuddy').scrollTop=it.rows[currentLine].offsetTop-offsetTr;
		}
	}
	changeBackground(){
		var it=$('buddyListTable');
		if(document.all){
			it=$('buddyListTable').children[0];
		}
		for (var i = 0;i<it.rows.length;i++) {
			if(i%2==0){
				it.rows[i].className="";
			}else{
				it.rows[i].className="";
			}
		}
	}
	constructor(props) {
		super(props);
	    this.state = {};
  	}
	componentDidUpdate(){

	}
	componentDidMount(){
       
	}
	render() {
		return(
			<table className="table" width="100%">
				<KeyHandler keyEventName={KEYPRESS} keyValue="w" onKeyHandle={this.onKeyUp.bind(this)} />
				<KeyHandler keyEventName={KEYPRESS} keyValue="s" onKeyHandle={this.onKeyDown.bind(this)} />
				<KeyHandler keyEventName={KEYPRESS} keyCode={13} onKeyHandle={this.onKeyEnter.bind(this)} />
				{this.props.children}
			</table>
		)
	}
}