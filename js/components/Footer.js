import React from 'react';
export default class Footer extends React.Component {
	handleAction(index){
		this.props.handleAction(index);
	}
	componentWillUpdate(nextProps,nextState){

			
  	}
  	componentDidUpdate(nextProps,nextState){

		/*
		//更新屏显
		if(typeof(python)!="undefined"){
			python.displayAblePrice(this.props.checkout.promotionPrice);
		}*/
  	}
	componentDidMount(){

		/*
		//更新屏显
        if(typeof(python)!="undefined"){
			python.displayAblePrice(this.props.checkout.promotionPrice);
		}*/

	}
	render() {
		return(
			<div className="footer">
				<div className="left message">
					<div className="details">
						<div>
							<p>合计:</p>
							<span>{this.props.checkout.promotionPrice}</span>
						</div>

						<div>
							<p>件数:</p>
							<span>{this.props.checkout.amount}</span>
						</div>

						<div>
							<p>已优惠:</p>
							<span>{this.props.checkout.discountPrice}</span>
						</div>
					</div>

					<div className="member">
						<div>
							<p>会员号:</p>
							<input type="hidden" id="memberId" value={this.props.member.leaguerId}/>
							<span>{this.props.member.leaguerId}</span>
						</div>

						<div>
							<p>类型:</p>
							<span>{this.props.member.leaguerType}</span>
						</div>

						<div>
							<p>积分:</p>
							<span>{this.props.member.leaguerIntegral}</span>
						</div>
					</div>
				</div>

				<div className="right total">
					<div>
						<p>应收金额：</p>
						<span>{this.props.checkout.promotionPrice}</span>
					</div>
					<button id="settlement" type="button" onClick={this.handleAction.bind(this,7)}>收银结算</button>
				</div>
			</div>
		)
	}
}