import React from 'react';
import KeyHandler, {KEYPRESS} from 'react-key-handler';


export default class CheckoutElement extends React.Component {

	onKeyEnter(e){
		var code=event.keyCode||event.whitch;
		if(code==13){
			if($('#abc').val().length>0){
				this.props.handleCheckout($('#abc').val());
			}
			
		}
	}
	
	constructor(props) {
		super(props);
	    this.state = {};
	    this.bound_onKeyEnter = this.onKeyEnter.bind(this);
  	}
	componentDidMount(){
		window.addEventListener("keyup",this.bound_onKeyEnter,false);
		 window.requestAnimationFrame(function() {
		 	$('#abc').focus();
		 });
	}
	componentDidUpdate(){
		
	}
	componentWillUnmount(){
		window.removeEventListener("keyup",this.bound_onKeyEnter,false);
		
	}
	
	render() {
		return(
			<div className="payment_method">
			
				<h2>收银结算</h2>
				<p>应付金额:</p>
				<span>{this.props.data.promotionPrice}元</span>
				<table>
					<tbody>
						<tr>
							<td>现金支付：</td>
							<td>
								<input id="abc"/>
							</td>
						</tr>

						<tr>
							<td>微信支付：</td>
							<td>
								<input/>
							</td>
						</tr>
					</tbody>
				</table>
				<p>ESC取消，ENTER确认</p>
			</div>
		)
	}
}