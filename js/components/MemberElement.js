import React from 'react';
import KeyHandler, {KEYPRESS} from 'react-key-handler';


export default class MemberElement extends React.Component {

	onKeyEnter(e){
		var code=event.keyCode||event.whitch;
		if(code==13){
			if($('#abc').val().length>0){
				this.props.handleMemberInfo($('#abc').val());
			}
			
		}
	}
	constructor(props) {
		super(props);
	    this.state = {};
	    this.bound_onKeyEnter = this.onKeyEnter.bind(this);
  	}
	componentDidMount(){
		window.addEventListener("keyup",this.bound_onKeyEnter,false);
		 window.requestAnimationFrame(function() {
		 	$('#abc').focus();
		 });
	}
	componentDidUpdate(){
		
	}
	componentWillUnmount(){
		window.removeEventListener("keyup",this.bound_onKeyEnter,false);
	}
	
	render() {
		return(
			<div className="payment_method">
			
				<h2>会员识别</h2>
				<table>
					<tbody>
						<tr>
							<td>用户标示：</td>
							<td>
								<input id="abc" placeholder="条码/手机号码"/>
							</td>
						</tr>

					</tbody>
				</table>
				<p>ESC取消，ENTER确认</p>
			</div>
		)
	}
}