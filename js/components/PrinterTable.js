import React from 'react';
export default class PrinterTable extends React.Component {
	
	componentDidMount(){
       
	}
	render() {
		var amount=0;
		var price=0;
		var products=this.props.data.map(function(product){
			var a=product.amount!=undefined?product.amount:1;
			amount+=parseInt(a);
			price+=parseInt(product.promotionPrice);
			return(
				<tr>
					<td width='130' align='left'>&nbsp;&nbsp;{product.productName}</td>
					<td width='30' align='left'>{a}</td>
					<td width='30' align='left'>{product.promotionPrice}</td>
				</tr>
			)
		}.bind(this));
		return(
			<div id="printerTable" className="hide">
				<table style={{fontSize:'20px'}}><tr><td width='190' align='center'>优客优奇</td></tr></table>
				<table><tr><td width='110'>&nbsp;&nbsp;收银员：xxx</td><td width='80'>店号：xxx</td></tr></table>
				<table><tr><td width='190'>&nbsp;&nbsp;时间：xxx</td></tr></table>
				<table>
					<tr>
						<td width='130' align='left'>&nbsp;&nbsp;商品名称</td>
						<td width='30' align='left'>数量</td>
						<td width='30' align='left'>单价</td>
					</tr>
					{products}
				</table>
				<table>
					<tr>
						<td width='60' align='left'>&nbsp;&nbsp;件数：{amount}</td>
						<td width='60' align='left'>&nbsp;&nbsp;金额：{price}</td>
					</tr>
				</table>
				<table>
					<tr>
						<td width='60' align='left'>&nbsp;&nbsp;应收：{price}</td>
						<td width='60' align='left'>&nbsp;&nbsp;实收：<span id="handMoney">0</span></td>
						<td width='60' align='left'>&nbsp;&nbsp;找零：<span id="backMoney">0</span></td>
					</tr>
				</table>

				<table>
					<tr>
						<td width='190' align='center'>欢迎下次光临</td>
					</tr>
				</table>
			</div>
		)
	}
}