import React from 'react';
import Api from '../network/DataService';
export default class Header extends React.Component {
	handleSignout(){
		Api.signout({},function(result){
			location.href="login.html";
		}.bind(this));
	}
	componentDidMount(){
      	
	}
	render() {
		return(
			<div>
				<div className="header">
					<h2>优客优奇收银系统</h2>
				</div>

				<div className="nav">
					<p>店名：</p>
					<span>{this.props.storename}</span>

					<p>收银员：</p>
					<span>{this.props.username}</span>

					<p>工号：</p>
					<span>{this.props.userid}</span>

					<a href="javascript:;" onClick={this.handleSignout.bind(this)} className="right nav_a">返回</a>
					<a href="" className="right">锁屏</a>
				</div>
			</div>
		)
	}
}