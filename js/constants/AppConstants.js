module.exports = {
  ADD_ITEM: 'ADD_ITEM',
  TIMEOUT: 'TIMEOUT',
  ERROR: 'ERROR',
  GET_ENTITY_DATA: 'GET_ENTITY_DATA',
  PENDING: 'PENDING',
  FB_OAUTH_TOKEN_SUCCESS: 'FB_OAUTH_TOKEN_SUCCESS',
  AUTH_LOG_IN: 'AUTH_LOG_IN',
  MODAL_STYLE :{
  		overlay : {
		    position          : 'fixed',
		    top               : 0,
		    left              : 0,
		    right             : 0,
		    bottom            : 0,
		    backgroundColor   : 'rgba(255, 255, 255,0.9)'
		},
		content : {
		    top                   : '50%',
		    left                  : '50%',
		    right                 : 'auto',
		    bottom                : 'auto',
		}
	},
};